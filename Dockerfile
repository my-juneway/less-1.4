FROM debian:9 AS build
RUN apt-get update && apt-get install -y wget make gcc libpcre3 libpcre3-dev zlib1g-dev libc6-dev \
    && mkdir /tmp/ngx_devel_kit && wget -qO- https://api.github.com/repos/vision5/ngx_devel_kit/tarball | tar zxf - -C /tmp/ngx_devel_kit --strip 1 \
    && mkdir /tmp/lua-nginx-module && wget -qO- https://api.github.com/repos/openresty/lua-nginx-module/tarball | tar zxf - -C /tmp/lua-nginx-module --strip 1 \
    && mkdir /tmp/luajit2 && wget -qO- https://api.github.com/repos/openresty/luajit2/tarball | tar zxf - -C /tmp/luajit2 --strip 1 \
    && cd /tmp/luajit2 && make install \
    && export LUAJIT_LIB=/usr/local/lib \
    && export LUAJIT_INC=/usr/local/include/luajit-2.1 \
    && mkdir -p /tmp/nginx && wget -qO- http://nginx.org/download/nginx-1.9.9.tar.gz | tar zxf - -C /tmp/nginx --strip 1 \
    && cd /tmp/nginx && ./configure \
        --prefix=/etc/nginx \
        --sbin-path=/usr/sbin/nginx \
        --conf-path=/etc/nginx/nginx.conf \
        --pid-path=/var/run/nginx.pid \
        --lock-path=/var/run/nginx.lock \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --with-ld-opt="-Wl,-rpath,/usr/local/lib" \
        --add-module=/tmp/ngx_devel_kit \
        --add-module=/tmp/lua-nginx-module \
    && make install \
    && mkdir /tmp/lua-resty-core && wget -qO- https://api.github.com/repos/openresty/lua-resty-core/tarball | tar zxf - -C /tmp/lua-resty-core --strip 1 \
    && cd /tmp/lua-resty-core && make install \
    && mkdir /tmp/lua-resty-lrucache && wget -qO- https://api.github.com/repos/openresty/lua-resty-lrucache/tarball | tar zxf - -C /tmp/lua-resty-lrucache --strip 1 \
    && cd /tmp/lua-resty-lrucache && make install \
    && mkdir /var/www && mv /etc/nginx/html /var/www \
    && sed -i 's/root   html/root   \/var\/www\/html/g' /etc/nginx/nginx.conf

FROM debian:9
COPY --from=build /usr/sbin/nginx /usr/sbin/nginx
COPY --from=build /usr/local/lib/libluajit* /usr/local/lib/
COPY --from=build /usr/local/lib/lua/resty /usr/local/share/lua/5.1/resty/
COPY --from=build /etc/nginx/ /etc/nginx/
COPY --from=build /var/www/ /var/www/
RUN mkdir -p /var/log/nginx && touch /var/log/nginx/error.log && touch /var/log/nginx/access.log && chmod +x /usr/sbin/nginx
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]